package repositories;

import domain.SiteApplication;

public interface SiteApplicationRepository {
	
	SiteApplication getApplicationByEmailAddress (String email);
	SiteApplication getApplicationByLogin (String login);
	void add(SiteApplication application);

}