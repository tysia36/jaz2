package repositories;

import java.util.List;
import java.util.ArrayList;

import domain.SiteApplication;


public class DummySiteApplicationRepository 
	implements SiteApplicationRepository
{
	private static List<SiteApplication> db
	= new ArrayList<SiteApplication>();
	
	@Override
	public SiteApplication getApplicationByEmailAddress (String email)
	{
		for (SiteApplication application: db)
		{
			if (application.getEmail().equalsIgnoreCase(email))
				return application;
		}
		return null;
	}
	
	@Override
	public SiteApplication getApplicationByLogin (String login)
	{
		for (SiteApplication application: db)
		{
			if (application.getLogin().equalsIgnoreCase(login))
				return application;
		}
		return null;
	}
	
	@Override
	public void add(SiteApplication application)
	{
		db.add(application);
	}

}