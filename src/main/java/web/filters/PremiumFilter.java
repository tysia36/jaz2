package web.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.SiteApplication;
import repositories.DummySiteApplicationRepository;
import repositories.SiteApplicationRepository;

@WebFilter({"/premium.jsp"})
public class PremiumFilter implements Filter{

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpSession session = httpRequest.getSession();
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		
		if(session.getAttribute("loged")!=null)
		{
			SiteApplicationRepository repository = new DummySiteApplicationRepository() {
			};
			SiteApplication known = repository.getApplicationByLogin((String)session.getAttribute("login"));
			if(known!=null)
			{
				if(known.getPremium().equals("no"))
					httpResponse.sendRedirect("/profile");
				else
					chain.doFilter(request, response);
			}
			else
				chain.doFilter(request, response);
			return;
		}
		chain.doFilter(request, response);
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}
