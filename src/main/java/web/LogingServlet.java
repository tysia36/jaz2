package web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.SiteApplication;
import repositories.DummySiteApplicationRepository;
import repositories.SiteApplicationRepository;

/**
 * Servlet implementation class LoggingServlet
 */
@WebServlet("/loged")
public class LogingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LogingServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.sendRedirect("/");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		SiteApplication application = retrieveApplicationFromRequest(request);
		SiteApplicationRepository repository = new DummySiteApplicationRepository() {
		};
		SiteApplication known = repository.getApplicationByLogin(application.getLogin());
		if(known!=null || application.getLogin().equals("admin"))
		{
			if (application.getLogin().equals("admin") && application.getPassword().equals("pass"))
			{
				session.setAttribute("login", application.getLogin());
				session.setAttribute("loged", "yes");
				response.sendRedirect("success.jsp");
			}
			else if (application.getLogin().equals(known.getLogin()) && application.getPassword().equals(known.getPassword()))
			{
				session.setAttribute("login", application.getLogin());
				session.setAttribute("loged", "yes");
				response.sendRedirect("success.jsp");
			}
			else
				response.sendRedirect("fail2.jsp");
		}
		else
		{
			response.sendRedirect("fail2.jsp");
		}
		
	}
	private SiteApplication retrieveApplicationFromRequest (HttpServletRequest request)
	{
		SiteApplication result = new SiteApplication();
		result.setLogin(request.getParameter("login"));
		result.setPassword(request.getParameter("password"));
		return result;
	}

}
