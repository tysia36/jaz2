package web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.SiteApplication;
import repositories.DummySiteApplicationRepository;
import repositories.SiteApplicationRepository;

/**
 * Servlet implementation class RegistringServlet
 */
@WebServlet("/registered")
public class RegistringServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegistringServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.sendRedirect("/");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		SiteApplication application = retrieveApplicationFromRequest(request);
		SiteApplicationRepository repository = new DummySiteApplicationRepository() {
		};
		if (repository.getApplicationByEmailAddress(application.getEmail())!=null || 
				repository.getApplicationByLogin(application.getLogin())!=null)
			response.sendRedirect("fail.jsp");
		else
		{
		application.setPremium("no");
		repository.add(application);
		session.setAttribute("login", application.getLogin());
		session.setAttribute("loged", "yes");
		response.sendRedirect("success2.jsp");
		}
	}
	private SiteApplication retrieveApplicationFromRequest (HttpServletRequest request)
	{
		SiteApplication result = new SiteApplication();
		result.setLogin(request.getParameter("login"));
		result.setPassword(request.getParameter("password"));
		result.setEmail(request.getParameter("mail"));
		return result;
	}

}
